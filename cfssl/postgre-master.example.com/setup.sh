#!/bin/bash

cd /opt/chef

chef-client -z -c /opt/chef/client.rb --runlist cfssl-pki::cert-db --json-attributes /opt/cfssl-training/cfssl/postgre-master.example.com/chef-attributes.json
