#!/bin/bash

cd /opt/chef

chef-client -z -c /opt/chef/client.rb \
  --runlist "cfssl-pki::default-cert-request,cfssl-pki::additional-cert-request" \
  --json-attributes \
    /opt/cfssl-training/cfssl/server-entity-additional.example.com/chef-attributes.json
