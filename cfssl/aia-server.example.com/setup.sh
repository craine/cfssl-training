#!/bin/bash

cd /opt/chef

chef-client -z -c /opt/chef/client.rb --runlist cfssl-pki::aia-server --json-attributes /opt/cfssl-training/cfssl/aia-server.example.com/chef-attributes.json
