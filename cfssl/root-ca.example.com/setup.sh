#!/bin/bash

cd /opt/cfssl/example-org
openssl genrsa -out root_ca-key.pem 4096
openssl req -x509 -new -nodes -key root_ca-key.pem -sha256 -days 3650 -out root_ca.pem

cp /opt/cfssl-training/cfssl/root-ca.example.com/config_root_ca.json /opt/cfssl/example-org/config_root_ca.json
