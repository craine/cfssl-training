#!/bin/bash

cfssl sign -ca intermediate_ca_01.pem -ca-key intermediate_ca_01-key.pem -config config_intermediate_ca_01.json issuing_ca_0101.csr | cfssljson -bare issuing_ca_0101
