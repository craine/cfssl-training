#!/bin/bash

cd /opt/chef

chef-client -z -c /opt/chef/client.rb \
  --runlist cfssl-pki::issuing-ca \
  --json-attributes \
    /opt/cfssl-training/cfssl/issuing-ca-0101.example.com/chef-attributes.json
