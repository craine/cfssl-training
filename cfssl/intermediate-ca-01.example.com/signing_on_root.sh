#!/bin/bash
cd /opt/cfssl/example-org/

cfssl sign -ca root_ca.pem -ca-key root_ca-key.pem -config config_root_ca.json intermediate_ca_01.csr | cfssljson -bare intermediate_ca_01
