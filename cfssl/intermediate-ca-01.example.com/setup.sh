#!/bin/bash

cd /opt/chef

chef-client -z -c /opt/chef/client.rb \
  --runlist cfssl-pki::intermediate-ca \
  --json-attributes \
    /opt/cfssl-training/cfssl/intermediate-ca-01.example.com/chef-attributes.json
