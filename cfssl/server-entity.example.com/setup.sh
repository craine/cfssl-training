#!/bin/bash

cd /opt/chef

chef-client -z -c /opt/chef/client.rb \
  --runlist cfssl-pki::default-cert-request \
  --json-attributes \
    /opt/cfssl-training/cfssl/server-entity.example.com/chef-attributes.json
