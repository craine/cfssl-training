#!/bin/bash

# Install Go
echo ">>>> Installing Go 1.7.5 <<<<"
cd /tmp
curl -O https://storage.googleapis.com/golang/go1.7.5.linux-amd64.tar.gz
tar -xvf go1.7.5.linux-amd64.tar.gz
mv go /usr/local

cat <<EOF >> /root/.bash_profile

# Go vars
export PATH=\$PATH:/usr/local/go/bin
export GOPATH=/opt/go
export PATH=\$PATH:\$GOPATH/bin
EOF
source /root/.bash_profile

# Install go.rice
go get github.com/GeertJohan/go.rice
go get github.com/GeertJohan/go.rice/rice
