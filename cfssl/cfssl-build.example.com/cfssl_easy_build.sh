#!/bin/bash
# Get and build CFSSL
echo ">>>> Installing CFSSL <<<<"
go get -u github.com/cloudflare/cfssl/cmd/...
mv /opt/go/bin/* /opt/cfssl/bin

cat 'export PATH=$PATH:/opt/cfssl/bin' >> /root/.bash_profile
source .bash_profile
