#!/bin/bash
# Clone the CFSSL repository
echo ">>>> Installing CFSSL <<<<"
cd /tmp
git clone https://github.com/cloudflare/cfssl.git
cd cfssl

# Embed Go & perform static Linux/amd64 build
pushd cli/serve && rice embed-go && popd
./script/build -os="linux" -arch="amd64"

# Remane files to remove build extension
cd ./dist
for file in *_linux-amd64
do
  mv -v "$file" "${file/_linux-amd64/}"
done

mkdir -p /opt/cfssl/bin
mv ./* /opt/cfssl/bin

cat <<EOF >> /root/.bash_profile

# CFSSL
export PATH=\$PATH:/opt/cfssl/bin
EOF
source /root/.bash_profile
