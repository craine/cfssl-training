#
# Cookbook Name:: cfssl-pki
# Recipe:: intermediate-ca
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

include_recipe 'cfssl-pki::trusted-cert-install'

template "/opt/cfssl/example-org/csr_intermediate_ca_#{node['pki']['intermediate_ca']}.json" do
  source 'csr_intermediate_ca.json.erb'
  mode '0644'
  owner 'root'
  group 'root'
  variables ca_number: node['pki']['intermediate_ca'],
            ip_address: node['ipaddress']
end

template "/opt/cfssl/example-org/config_intermediate_ca_#{node['pki']['intermediate_ca']}.json" do
  source 'config_intermediate_ca.json.erb'
  mode '0644'
  owner 'root'
  group 'root'
  variables ca_number: node['pki']['intermediate_ca']
end

execute 'generate csr and private key' do
  command "cfssl genkey csr_intermediate_ca_#{node['pki']['intermediate_ca']}.json |
    cfssljson -bare intermediate_ca_#{node['pki']['intermediate_ca']}"
  cwd '/opt/cfssl/example-org'
  not_if { ::File.exist?("/opt/cfssl/example-org/intermediate_ca_#{node['pki']['intermediate_ca']}.csr") }
end
