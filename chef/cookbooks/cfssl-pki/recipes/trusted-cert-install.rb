#
# Cookbook Name:: cfssl-pki
# Recipe:: trusted-cert-install
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

directory '/usr/local/share/ca-certificates/example-org/' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

# Installs the Root CA certificate. Required for all servers
cookbook_file '/usr/local/share/ca-certificates/example-org/root-ca.crt' do
  source 'root_ca.pem'
  mode '0755'
  owner 'root'
  group 'root'
end

execute 'update system ca store' do
  command 'update-ca-certificates'
end
