#
# Cookbook Name:: cfssl-pki
# Recipe:: cert-db
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

include_recipe 'cfssl-pki::trusted-cert-install'

db_info_hash = node['pki']['db_info']
api_key = '1234567890ACBDEF1234567890ABCDEF'

if node['pki']['role'] == 'cert_db_master'
  include_recipe 'ha-postgresql::ha-master'
elsif node['pki']['role'] == 'cert_db_slave'
  include_recipe 'ha-postgresql::ha-slave'
else
  puts 'no database role selected. the node[\'pki\'][\'role\'] attribute must be set to either `cert_db_master` or `cert_db_slave`.'
  return
end

hba_line = {
  comment: '# non-hostssl access for initial cfssl db setup & cert pull for this server',
  type: 'host',
  db: db_info_hash['database']['db_name'],
  user: db_info_hash['database']['username'],
  addr: db_info_hash['issuing_ca'],
  method: 'password'
}

node.default['postgresql']['config']['listen_addresses'] = "0.0.0.0"

# Get an SSL certificate for the Nginx server proxy
hostname = "#{node['hostname']}.example.com"
ca_servers = "https://#{db_info_hash['issuing_ca'].slice(0, db_info_hash['issuing_ca'].length - 3)}"


if !::File.exist?("/opt/cfssl/example-org/#{hostname}")
  node.default['postgresql']['pg_hba'].push(hba_line)

  service 'postgresql' do
    action :reload
  end

  directory "/opt/cfssl/example-org/#{hostname}/" do
    owner 'root'
    group 'root'
    mode '0755'
    recursive true
    action :create
  end

  template "/opt/cfssl/example-org/#{hostname}/csr.json" do
    source 'csr_server.json.erb'
    owner 'root'
    group 'root'
    mode '0600'
    variables hostname: hostname,
              ip_address: node['network']['interfaces']['eth1']['routes'][0]['src']
  end

  template "/opt/cfssl/example-org/#{hostname}/config_end_entity_req.json" do
    source 'config_server.json.erb'
    owner 'root'
    group 'root'
    mode '0600'
    variables api_key: api_key,
              issuing_ca: ca_servers
  end
elsif ::File.exist?("/opt/cfssl/example-org/#{hostname}")
  node.default['postgresql']['pg_hba'].delete(hba_line)

  cert_chain = [
    "intermediate_ca_#{node['pki']['intermediate_ca']}.pem",
    "issuing_ca_#{node['pki']['intermediate_ca']}#{node['pki']['issuing_ca']}.pem"
  ]

  cert_chain.each do |cert|
    cookbook_file "/opt/cfssl/example-org/#{hostname}/#{cert}" do
      source cert
      owner 'root'
      group 'root'
      mode '644'
    end
  end

  if ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") && (renew_cert? "/opt/cfssl/example-org/#{hostname}/cert.pem")
    directory "/opt/cfssl/example-org/#{hostname}/archive" do
      owner 'root'
      group 'root'
      mode '0755'
      recursive true
      action :create
    end

    files = [
      'cert.pem',
      'cert-key.pem',
      'cert.csr',
      'cert_bundle.pem'
    ]

    files.each do |file|
      execute 'move_to_archive' do
        cwd "/opt/cfssl/example-org/#{hostname}"
        command "mv -f #{file} archive/#{file}"
      end
    end
  end

  execute 'gencert_and_sign' do
    command "cfssl gencert -remote #{ca_servers} -profile server -config config_end_entity_req.json csr.json | cfssljson -bare cert"
    cwd "/opt/cfssl/example-org/#{hostname}"
    not_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") }
  end

  execute 'bundle_certs' do
    command "cat cert.pem #{cert_chain[1]} #{cert_chain[0]} > cert_bundle.pem"
    cwd "/opt/cfssl/example-org/#{hostname}"
    only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") }
  end

  node.default['postgresql']['config']['ssl_cert_file'] = "/opt/cfssl/example-org/#{hostname}/cert_bundle.pem"
  node.default['postgresql']['config']['ssl_key_file'] = "/opt/cfssl/example-org/#{hostname}/cert-key.pem"
end

template '/var/lib/postgresql/.pgpass' do
  source 'pgpass.erb'
  owner 'postgres'
  group 'postgres'
  mode '0600'
  variables password: db_info_hash['database']['password']
end

cookbook_file '/tmp/cert_db.sql' do
  source 'cert_db.sql'
  owner 'postgres'
  group 'postgres'
  mode '0600'
  not_if { tables_created? }
end

ruby_block 'create_tables' do
  block do
    create_tables?
  end
  not_if { tables_created? }
end
