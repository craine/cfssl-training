#
# Cookbook Name:: cfssl-pki
# Recipe:: server-cert-request
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

include_recipe 'cfssl-pki::trusted-cert-install'
api_key = '1234567890ACBDEF1234567890ABCDEF'

node['pki']['additional_certificates'].each do |host|
  hostname = "#{host}"
  ca_servers = "https://#{node['pki']['issuing_ca_ip']}"

  directory "/opt/cfssl/example-org/#{hostname}" do
    owner 'root'
    group 'root'
    mode '0755'
    action :create
  end

  template "/opt/cfssl/example-org/#{hostname}/csr.json" do
    source 'csr_server.json.erb'
    owner 'root'
    group 'root'
    mode '0755'
    variables hostname: hostname,
              ip_address: node['ipaddress']
  end

  template "/opt/cfssl/example-org/#{hostname}/config.json" do
    source 'config_server.json.erb'
    owner 'root'
    group 'root'
    mode '0755'
    variables api_key: api_key,
              issuing_ca: ca_servers
  end

  if ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") && (renew_cert? "/opt/cfssl/example-org/#{hostname}/cert.pem")
    directory "/opt/cfssl/example-org/#{hostname}/archive" do
      owner 'root'
      group 'root'
      mode '0755'
      recursive true
      action :create
    end

    files = [
      'cert.pem',
      'cert-key.pem',
      'cert.csr',
      'cert_bundle.pem'
    ]

    files.each do |file|
      execute 'move_to_archive' do
        cwd "/opt/cfssl/example-org/#{hostname}"
        command "mv -f #{file} archive/#{file}"
      end
    end
  end

  execute 'gencert_and_sign' do
    command "cfssl gencert -remote=#{ca_servers} -profile server -config config.json csr.json | cfssljson -bare cert"
    cwd "/opt/cfssl/example-org/#{hostname}"
    not_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") }
  end

  cert_chain = [
    'intermediate_ca_01.pem',
    'issuing_ca_0101.pem'
  ]

  cert_chain.each do |cert|
    cookbook_file "/opt/cfssl/example-org/#{hostname}/#{cert}" do
      source cert
      owner 'root'
      group 'root'
      mode '644'
    end
  end

  execute 'bundle_certs' do
    command "cat cert.pem #{cert_chain[1]} #{cert_chain[0]} > cert_bundle.pem"
    cwd "/opt/cfssl/example-org/#{hostname}"
    only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") }
  end

  execute 'drop_ca_bundle' do
    command "cat /opt/cfssl/example-org/#{hostname}/#{cert_chain[1]} /opt/cfssl/example-org/#{hostname}/#{cert_chain[0]}  /usr/local/share/ca-certificates/example-org/root-ca.crt > ca_bundle.pem"
    cwd "/opt/cfssl/example-org"
    not_if { ::File.exist?("/opt/cfssl/example-org/ca_bundle.pem") }
  end

end
