#
# Cookbook Name:: cfssl-pki
# Recipe:: issuing-ca
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

include_recipe 'cfssl-pki::trusted-cert-install'

db_info_hash = node['pki']['db_info']
api_key = '1234567890ACBDEF1234567890ABCDEF'

# Account for IP addresses or Hostnames in the db_info_hash
master_host = if db_info_hash['ha']['master_node'].slice(db_info_hash['ha']['master_node'].length - 3, 3) == '/32'
                db_info_hash['ha']['master_node'].slice(0, db_info_hash['ha']['master_node'].length - 3)
              else
                db_info_hash['ha']['master_node']
              end

# Create the configuration and certifiate JSON files that CFSSL will use
template '/opt/cfssl/example-org/csr_issuing_ca.json' do
  source 'csr_issuing_ca.json.erb'
  mode '0644'
  owner 'root'
  group 'root'
  variables parent_ca_number: node['pki']['intermediate_ca'],
            issuing_ca_number: node['pki']['issuing_ca'],
            ip_address: node['ipaddress']
end

template '/opt/cfssl/example-org/config_issuing_ca.json' do
  source 'config_issuing_ca.json.erb'
  mode '0644'
  owner 'root'
  group 'root'
  variables parent_ca_number: node['pki']['intermediate_ca'],
            issuing_ca_number: node['pki']['issuing_ca'],
            api_key: api_key
end

template '/opt/cfssl/example-org/cert_db.json' do
  source 'cert_db.json.erb'
  mode '0644'
  owner 'root'
  group 'root'
  variables db_password: db_info_hash['database']['password'],
            db_hostname: master_host
end

# Generate the CSR and private key based on the files we just dropped
execute 'generate csr and private key' do
  command 'cfssl genkey csr_issuing_ca.json | cfssljson -bare issuing_ca'
  cwd '/opt/cfssl/example-org'
  not_if { ::File.exist?('/opt/cfssl/example-org/issuing_ca.csr') }
end

# Make CFSSL into a service with an Upstart config
cookbook_file '/etc/init/cfssld.conf' do
  source 'cfssld.conf'
  mode '0644'
  owner 'root'
  group 'root'
  only_if { ::File.exist?('/opt/cfssl/example-org/issuing_ca.pem') }
end

service 'cfssld' do
  action :restart
  only_if { ::File.exist?('/etc/init/cfssld.conf') }
end

hostname = "#{node['hostname']}.example.com"

directory "/opt/cfssl/example-org/#{hostname}/" do
  owner 'root'
  group 'root'
  mode '0755'
  recursive true
  action :create
end

template "/opt/cfssl/example-org/#{hostname}/csr.json" do
  source 'csr_server.json.erb'
  owner 'root'
  group 'root'
  mode '0600'
  variables hostname: hostname,
            ip_address: node['network']['interfaces']['eth1']['routes'][0]['src']
end

cert_sign_host = '127.0.0.1:8888'

template "/opt/cfssl/example-org/#{hostname}/config_end_entity_req.json" do
  source 'config_server.json.erb'
  owner 'root'
  group 'root'
  mode '0600'
  variables api_key: api_key,
            issuing_ca: cert_sign_host
end

if ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") && (renew_cert? "/opt/cfssl/example-org/#{hostname}/cert.pem")
  directory "/opt/cfssl/example-org/#{hostname}/archive" do
    owner 'root'
    group 'root'
    mode '0755'
    recursive true
    action :create
  end

  files = [
    'cert.pem',
    'cert-key.pem',
    'cert.csr',
    'cert_bundle.pem'
  ]

  files.each do |file|
    execute 'move_to_archive' do
      cwd "/opt/cfssl/example-org/#{hostname}"
      command "mv -f #{file} archive/#{file}"
    end
  end
end

execute 'gencert_and_sign' do
  command "cfssl gencert -remote #{cert_sign_host} -profile server -config config_end_entity_req.json csr.json | cfssljson -bare cert"
  cwd "/opt/cfssl/example-org/#{hostname}"
  not_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") }
  only_if { ::File.exist?('/etc/init/cfssld.conf') }
end

cert_chain = [
               'intermediate_ca_01.pem',
               'issuing_ca_0101.pem'
             ]

cert_chain.each do |cert|
  cookbook_file "/opt/cfssl/example-org/#{hostname}/#{cert}" do
    source cert
    owner 'root'
    group 'root'
    mode '644'
    only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") }
  end
end

execute 'bundle_certs' do
  command "cat cert.pem #{cert_chain[1]} #{cert_chain[0]} > cert_bundle.pem"
  cwd "/opt/cfssl/example-org/#{hostname}"
  only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") }
end

# Install & configure Nginx to act as a proxy for the service
package 'nginx' do
  action :upgrade
end

service 'nginx' do
  action :stop
end

template '/etc/nginx/sites-available/issuing_ca.conf' do
  source 'issuing_ca_nginx.conf.erb'
  mode '0744'
  owner 'root'
  group 'root'
  variables hostname: hostname
  only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert.pem") }
  only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert-key.pem") }
end

directory "/var/log/nginx/#{hostname}/http" do
  owner 'root'
  group 'root'
  mode '0744'
  recursive true
  action :create
end

directory "/var/log/nginx/#{hostname}/https" do
  owner 'root'
  group 'root'
  mode '0744'
  recursive true
  action :create
end

execute 'enable_nginx_config' do
  command 'ln -s /etc/nginx/sites-available/issuing_ca.conf /etc/nginx/sites-enabled/issuing_ca.conf'
  not_if { ::File.exist?('/etc/nginx/sites-enabled/issuing_ca.conf') }
  only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/cert-key.pem") }
end

file '/etc/nginx/sites-enabled/default' do
  action :delete
  force_unlink true
  only_if { ::File.exist?('/etc/nginx/sites-enabled/default') }
end

service 'nginx' do
  action :restart
  only_if { ::File.exist?('/etc/nginx/sites-enabled/issuing_ca.conf') }
end
