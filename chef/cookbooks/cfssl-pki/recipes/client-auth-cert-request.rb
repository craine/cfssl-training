#
# Cookbook Name:: cfssl-pki
# Recipe:: server-cert-request
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

include_recipe 'cfssl-pki::trusted-cert-install'
api_key = '1234567890ACBDEF1234567890ABCDEF'

node.default['pki']['client_auth']['domain'] = 'example.com'
hostname = "#{node['hostname']}.#{node['pki']['client_auth']['domain']}"
ca_servers = "https://#{node['pki']['issuing_ca_ip']}"

directory "/opt/cfssl/example-org/#{hostname}" do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

directory "/opt/cfssl/example-org/#{hostname}/client_auth" do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

template "/opt/cfssl/example-org/#{hostname}/client_auth/csr.json" do
  source 'csr_server.json.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables hostname: hostname,
            ip_address: node['ipaddress']
end

template "/opt/cfssl/example-org/#{hostname}/client_auth/config.json" do
  source 'config_client.json.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables api_key: api_key,
            issuing_ca: ca_servers
end

if ::File.exist?("/opt/cfssl/example-org/#{hostname}/client_auth/cert.pem") && (renew_cert? "/opt/cfssl/example-org/#{hostname}/client_auth/cert.pem")
  directory "/opt/cfssl/example-org/#{hostname}/client_auth/archive" do
    owner 'root'
    group 'root'
    mode '0755'
    recursive true
    action :create
  end

  files = [
    'cert.pem',
    'cert-key.pem',
    'cert.csr',
    'cert_bundle.pem'
  ]

  files.each do |file|
    execute 'move_to_archive' do
      cwd "/opt/cfssl/example-org/#{hostname}/client_auth"
      command "mv -f #{file} archive/#{file}"
    end
  end
end

execute 'gencert_and_sign' do
  command "cfssl gencert -remote=#{ca_servers} -profile client -config config.json csr.json | cfssljson -bare cert"
  cwd "/opt/cfssl/example-org/#{hostname}/client_auth"
  not_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/client_auth/cert.pem") }
end

cert_chain = [
  'intermediate_ca_02.pem',
  'issuing_ca_0201.pem'
]

cert_chain.each do |cert|
  cookbook_file "/opt/cfssl/example-org/#{hostname}/client_auth/#{cert}" do
    source cert
    owner 'root'
    group 'root'
    mode '644'
  end
end

execute 'bundle_certs' do
  command "cat cert.pem #{cert_chain[1]} #{cert_chain[0]} > cert_bundle.pem"
  cwd "/opt/cfssl/example-org/#{hostname}/client_auth"
  only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/client_auth/cert.pem") }
end

execute 'create_ca_bundle' do
  command "cat #{cert_chain[1]} #{cert_chain[0]} /usr/local/share/ca-certificates/distil/root_ca.crt > ca_bundle.pem"
  cwd "/opt/cfssl/example-org/#{hostname}/client_auth"
  only_if { ::File.exist?("/opt/cfssl/example-org/#{hostname}/client_auth/cert.pem") }
end
