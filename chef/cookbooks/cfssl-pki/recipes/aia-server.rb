#
# Cookbook Name:: cfssl-pki
# Recipe:: issuing-ca
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

include_recipe 'cfssl-pki::trusted-cert-install'

# Configure Nginx for serving the certificates and CRLs
package 'nginx' do
  action :upgrade
end

directories = [
  '/usr/share/nginx/internal-pki.example.com',
  '/usr/share/nginx/internal-pki.example.com/certs',
  '/usr/share/nginx/internal-pki.example.com/crls',
  '/var/log/nginx/http'
]

certs = [
  'root_ca.pem',
  'intermediate_ca_01.pem',
  'issuing_ca_0101.pem',
]

crls = []

cookbook_file '/etc/nginx/sites-available/internal-pki.example.com.conf' do
  source 'aia_nginx.conf'
  owner 'www-data'
  group 'www-data'
  mode '0755'
end

execute 'enable_nginx_config' do
  command 'ln -s /etc/nginx/sites-available/internal-pki.example.com.conf /etc/nginx/sites-enabled/internal-pki.example.com.conf'
  not_if { ::File.exist?('/etc/nginx/sites-enabled/internal-pki.example.com.conf') }
end

file '/etc/nginx/sites-enabled/default' do
  action :delete
  force_unlink true
  only_if { ::File.exist?('/etc/nginx/sites-enabled/default') }
end

service 'nginx' do
  action :restart
end

directories.each do |dir|
  directory dir do
    owner 'www-data'
    group 'www-data'
    mode '0555'
  end
end

certs.each do |cert|
  cookbook_file "/usr/share/nginx/internal-pki.example.com/certs/#{cert}" do
    owner 'www-data'
    group 'www-data'
    mode '0555'
  end
end

crls.each do |crl|
  cookbook_file "/usr/share/nginx/internal-pki.example.com/crls/#{crl}" do
    owner 'www-data'
    group 'www-data'
    mode '0555'
  end
end
