name 'cfssl-pki'
maintainer 'Craine Runton'
maintainer_email 'craine@runtondev.com'
license 'MIT'
description 'Installs/Configures various PKI CA servers'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.0.0'

depends 'ha-postgresql'
