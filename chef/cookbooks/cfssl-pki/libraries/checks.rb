require 'mixlib/shellout'
require 'openssl'
require 'date'

def tables_created?
  table_list = Mixlib::ShellOut.new('sudo PGPASSFILE="/var/lib/postgresql/.pgpass" psql -U cfssl -w -c "\dt"')
  table_list.run_command
  table_list.stdout.include?('certificates') ? true : false
end

def create_tables?
  tables_created = Mixlib::ShellOut.new('sudo PGPASSFILE="/var/lib/postgresql/.pgpass" psql -U cfssl -w -c "\i /tmp/cert_db.sql;"')
  tables_created.run_command
  tables_created.stdout.include?('CREATE TABLE') ? true : false
end

def renew_cert?(cert_path)
  raw = File.read cert_path
  certificate = OpenSSL::X509::Certificate.new raw
  exp_date = Date.parse certificate.not_after.to_s
  today = Date.today
  (today + 30) > exp_date ? true : false
end
