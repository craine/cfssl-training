cfssl-pki Cookbook
================

Creates servers used in an internal PKI

Requirements
------------

Assumes that CFSSL has been installed on the server in question

Usage
-----

# Table of Contents
* [PKI Components and Recipes](#pki-components-and-recipes)
  * [Intermediate CA](#intermediate-ca)
  * [Issuing CA](#issuing-ca)
  * [Certificate Databases](#certificate-databases)
    * [Master Nodes](#master-nodes)
    * [Slave Nodes](#slave-nodes)
  * [AIA Server](#aia-server)
  * [End-Entity Servers](#end-entity-servers)
    * [Default Certificates](#default-certificates)
    * [Additional Certificates](#additional-certificates)
    * [Client Authentication Certificates](#client-authentication-certificates)
* [Examples](#examples)
  * [Creating a new Issuing CA Stack](#creating-a-new-issuing-ca-stack)
  * [Manually Requesting a Certificate](#manually-requesting-a-certificate)

# PKI Components and Recipes
## Intermediate CAs
### Requirements
**Chef Runlist**
Add `cfssl-pki::intermediate-ca` to the node's runlist.

**Chef Node Attribute**
When setting up a new Intermediate CA, you need to use the following node attributes:
```json
{
  "pki": {
    "role":"intermediate_ca",
    "intermediate_ca": "{{new_ca_number}}",
    }
}
```
where `{{new_ca_number}}` is the next number in the Intermediate CA series.

### Usage
Once the chef-client run is complete, you will find the following files within the `/opt/cfssl/example-org` directory:
```
intermediate_ca_{{new_ca_number}}.csr
intermediate_ca_{{new_ca_number}}-key.pem
```
The `CSR` will need to be transferred off the machine and signed by the Root CA.

Once you have a signed certificate back from the Root CA, you will need to put it in the same `/opt/cfssl/example-org` directory the other files are located in. You will also need to copy the certificate into the `files/default` directory for this cookbook using the same filename as returned from the Root CA (`intermediate_ca_{{new_ca_number}}.pem`).


## Issuing CAs
### Requirements
**Chef Runlist**
Add `cfssl-pki::issuing-ca` to the node's runlist.

**Chef Node Attribute**
When setting up a new Issuing CA, you need to add the following node attribute:
```json
{
  "pki": {
    "role":"issuing_ca",
    "intermediate_ca": "{{parent_ca_number}}",
    "issuing_ca": "{{new_ca_number}}"
  },
  "database_role": "issuing_ca_{{number}}_certdb",
}
```
where `{{new_ca_number}}` is the next number in the Issuing CA series for the parent Intermediate CA (noted by `{{parent_ca_number}}`). We are including the `database_role` attribute so we have access to the Vault secrets with the password for the cfssl user that is setup for this particular Issuing CA.

### Usage
Once the chef-client run is complete, you will find the following files within the `/opt/cfssl/example-org` directory:
```
intermediate_ca.csr
intermediate_ca-key.pem
```
The `CSR` will need to be transferred off the machine and signed by the Root CA. Contact Craine Runton or Chris Nelson about getting this done. The private key file will be ~~uploaded into Hashicorp Vault for secure storage, and then deleted from disk~~ kept on disk until Vault integration is complete.

Once you have a signed certificate back from the Root CA, you will need to put it in the same directory the other files are located in. You will also need to copy the certificate into the `files/default` directory for this cookbook using the same filename as returned from the Root CA (`intermediate_ca.pem`), and place it's MD5 hash into the `cert_md5sums` array with the base filename as the key.



## Certificate Databases
For redundancy and availability, we are using clustered PostgreSQL servers, operating in a Hot Standby/streaming replication setup. For more information on the actual setup of the Postgres cluster, please see the [postgresql cookbook](1). Note that you do not need to include the postgres recipes in your cert-db's Chef runlist, as it is a dependency in these recipes, and based on the Chef node attributes.

### Master Nodes
**Chef Runlist**
Add `cfssl-pki::cert-db` to the node's runlist.

**Chef Node Attribute**
When setting up a new Certificate database master, you need to add the following node attributes:
```json
{
  "database_role": "issuing_ca_{{number}}_certdb",
  "pki": {
    "role":"cert_db_master",
    "intermediate_ca": "{{intermediate_ca_number}}",
    "issuing_ca": "{{issuing_ca_number}}"
    }
}
```

**Chef Vault secrets**
Since we are using a PostgreSQL cluster for the certificate databases, we will need to generate the Chef Vault secrets that will be used for the server setup. Below is the format that we need to follow for our secret:  
```json
{
  "database": {
    "db_name":"cfssl",
    "username": "cfssl",
    "password":"{{db_password}}"
  },
  "ha": {
    "master_node": "{{ipaddress_for_master_node}}",
    "slave_node": "{{ipaddress_for_slave_node}}",
    "replication_password": "{{password_for_replication_user}}"
  }
}
```
In the above example, you will want to create two separate (long, random, secure) passwords for Postgres, one for the actual `cfssl` database user, and one for the replication user. You will also need to know the IP addresses of the master and slave nodes of the Postgres cluster. **NOTE: When using an IP address, you must include the netmask of /32 at the end of the IP.**  

### Slave Nodes
The Slave node receives streaming write ahead transaction logs from the master node as the master node processes database transactions. You **MUST** have the master node configured and operational before attempting to run the slave node's initial Chef run. Do not attempt to run `chef-client` simultaneously on both the master and the slave. It will not work correctly.

**Chef Runlist**
Add `cfssl-pki::cert-db` to the node's runlist.

**Chef Node Attribute**
When setting up a new Certificate database master, you need to add the following node attributes:
```json
{
  "database_role": "issuing_ca_{{number}}_certdb",
  "pki": {
    "role":"cert_db_slave",
    "intermediate_ca": "{{parent_ca_number}}",
    "issuing_ca": "{{new_ca_number}}"
    }
}
```

**Chef Vault secrets**
Since we are using a PostgreSQL cluster for the certificate databases, we will need to generate the Chef Vault secrets that will be used for the server setup. Below is the format that we need to follow for our secret:  
```json
{
  "database": {
    "db_name":"cfssl",
    "username": "cfssl",
    "password":"{{db_password}}"
  },
  "ha": {
    "master_node": "{{ipaddress_for_master_node}}",
    "slave_node": "{{ipaddress_for_slave_node}}",
    "replication_password": "{{password_for_replication_user}}"
  }
}
```
In the above example, you will want to create two separate (long, random, secure) passwords for Postgres, one for the actual `cfssl` database user, and one for the replication user. You will also need to know the IP addresses of the master and slave nodes of the Postgres cluster. **NOTE: When using an IP address, you must include the netmask of /32 at the end of the IP.**  

## AIA Server
The Authority Information Access server is a web server that makes all of the PKI certificates and certificate revocation lists available in one place. Each certificate has an AIA endpoint and CRL endpoint encoded into it. These endpoints are served up via HTTP (important to not use HTTPS, or you risk introducing circular logic loops) on the AIA server. There *should* only be two AIA servers, one in Production at [internal-pki.example.com] and one in Staging at [internal-pki.staging.distil.it]. Do not assign this role to additional servers unless you are rebuilding one of those two servers.

**Chef Runlist**
Add `cfssl-pki::aia-server` to the node's runlist.

**Chef Node Attribute**
When setting up a new AIA Server, you need to add the following node attributes:
```json
{
  "pki": {
    "role": "aia_server"
  }
}
```

## End-Entity Servers
End-entity servers are the consumers of the PKI in the server infrastructure. They are your web app, database, etc. servers. End-entity servers will get a default certificate issued by the PKI for the hostname (as identified in OpenStack) and the IP address (as identified by Chef node attributes) of the machine.

### Default Certificates
The default certificate request is handled by the default Chef runlist. There is no need to add anything additional to the runlist or the node attributes. Default certificates and keys are dropped into the `/opt/cfssl/example-org/{{hostname}}/` directory. Please do *not* move them from this location. If you need to use them elsewhere on the system (due to hard-coded paths or something) please *copy* the files.

Default certificates expire every 90 days, and are renewed every 75 days. The certificates, private keys, and certificate bundles are all regenerated. The expiring files are moved into the `/opt/cfssl/example-org/{{hostname}}/archive` directory. Only the most recent version is kept in the `archive` directory.

### Additional Certificates
If you are running a web application, database, or something else and need a `TLS Web Server` type certificate for an additional hostname (if your web app is on a different hostname than your server, for example), you can request additional certificates for that.

**Chef Runlist**
Add `cfssl-pki::additional-cert-request` to the node's runlist.

**Chef Node Attribute**
When setting up a new AIA Server, you need to add the following node attributes:
```json
{
  "pki": {
    "role": "end_entity",
    "additional_certificates": [
      "{{hostname-1}}",
      "{{hostname-2}}",
      "{{hostname-N}}"
    ]
  }
}
```
The additional certificates and matching keys will be placed in the `/opt/cfssl/example-org/{{hostname-1}}/` directory for your use. As with the default certs & keys, please do not move them out of this directory. Copy them to another location if you need to use them elsewhere on the file system.

**NOTE:** Do *not* include `.distil.it` or `.staging.distil.it` in the hostname (thus making it an FQDN). The recipe handles that automatically based on the Chef environment.

Additional certificates expire every 90 days, and are renewed every 75 days. The certificates, private keys, and certificate bundles are all regenerated. The expiring files are moved into the `/opt/cfssl/example-org/{{hostname-1}}/archive` directory. Only the most recent version is kept in the `archive` directory.

### Client Authentication Certificates
If for some reason you need to do mutual server-client TLS authentication between two servers (e.g. you have a server-client architecture distributed across multiple servers), and you need to obtain a certificate with an EKU for `TLS Web Client Authentication`, then you need to add the `cfssl-pki::client-auth-cert-request` recipe to your run list _after_ the `wrappers::baseline-production` or `wrapper::baseline-staging` recipe. These certificates are valid for 90 days, and will renew when `chef-client` is ran within 30 days of certificate expiration.

**Optional Chef Node Attributes**
If you are using an FQDN that is not in the `distil.it` or `staging.distil.it` domain, then you will need to supply an additional override attribute on the Chef node, like so:
```json
{
  "pki": {
    "client_auth": {
      "domain": "distil.us"
    }
  }
}
```

Available domains that will be signed by the Issuing CAs include:
**Production**
```
distil.it
distil.us
distilnetworks.com
```
**Staging**
```
staging.distil.it
staging.distil.us
staging.distilnetworks.com
```

The certificates generated from this recipe will be placed in `/opt/cfssl/example-org/{{fqdn}}/client_auth`. It will include the following files:
```
cert.pem      <-- Client Authentication certificates
cert-key.pem  <-- Private key
cert.csr      <-- Cert signing request

cert_bundle.pem     <-- Includes the cert.pem, issuing CA cert, intermediate CA cert
ca_bundle.pem       <-- Includes the Issuing CA cert, Intermediate CA cert, and Root CA cert
intermediate_ca_{{ca_number}}.pem     <-- The cert from the Intermediate CA over the Issuing CA
issuing_ca_{{ca_number}}.pem          <-- The cert from the Issuing CA that issued the client certificate
```

# Examples
## Creating a new Issuing CA Stack
### Requirements
When you need to create a new Issuing CA stack, you need to create the following servers:
* Issuing CA
* Cert-DB master node
* Cert-DB slave node

## Manually Requesting a Certificate
If for some reason you need to obtain certificates for a machine that isn't managed by Chef, you are able to manually request certificates. In order to do so, you must make sure that you have the following pre-requisites fulfilled:
* CFSSL binaries on the machine that you intend to make the request from
* All required attributes for the machine you are requesting for, including:
  * `Common Name`
  * `hostname`
  * `SANs`
  * `IP address`



[1]: https://github.com/distil/chef-kitchen/tree/master/cookbooks/postgresql
