# Establish default database name
default['postgresql']['database_name'] = 'postgres'

# Setup postgresql.conf options for daemon listening address and replication options
node.default['postgresql']['config']['listen_addresses'] = "localhost, #{node['ipaddress']}"
node.default['postgresql']['config']['hot_standby'] = true

# Let the postgresql recipe tune the install for use as a web service
node.default['postgresql']['config_pgtune']['db_type'] = 'web'
