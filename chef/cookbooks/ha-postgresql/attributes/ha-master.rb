# Establish default database name
default['postgresql']['database_name'] = 'postgres'

# Setup postgresql.conf options for daemon listening address and replication options
default['postgresql']['config']['listen_addresses'] = "localhost, #{node['ipaddress']}"
default['postgresql']['config']['wal_level'] = 'hot_standby'
default['postgresql']['config']['archive_mode'] = true
default['postgresql']['config']['archive_command'] = 'test ! -f /mnt/server/archivedir/%f && cp %p /mnt/server/archivedir/%f'
default['postgresql']['config']['max_wal_senders'] = 3

# Let the postgresql recipe tune the install for use as a web service
node.default['postgresql']['config_pgtune']['db_type'] = 'web'
