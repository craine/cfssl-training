postgresql CHANGELOG
===============

This file is used to list changes made in each version of the PostgreSQL cookbook.

=====

1.0.0
-----
- Craine Runton - Initial release of HA-PostgreSQL

- - -
