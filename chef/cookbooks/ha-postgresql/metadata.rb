name 'ha-postgresql'
maintainer 'Craine Runton'
maintainer_email 'craine@runtondev.com'
license 'MIT'
description 'Installs/Configures PostgreSQL server in an HA setup'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.0.0'

depends 'postgresql'
depends 'database'
