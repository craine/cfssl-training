resource_name :postgres_ha

property :database, String, name_property: true
property :username, String
property :password, String

default_action :bootstrap_db

action :bootstrap_db do
  create_database? database unless database_created? database
  create_user? username, password unless username_created? username
  grant_privileges? database, username unless privs_granted? username
end

action :replication_user do
  create_replication_user? password
end
