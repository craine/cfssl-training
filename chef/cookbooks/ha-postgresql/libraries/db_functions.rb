require 'mixlib/shellout'

def login_available?(database, username)
  database_created = database_created?(database)
  username_created = username_created?(username)
  grant_created = privs_granted?(username)

  !database_created || !username_created || !grant_created ? false : true
end

def database_created?(database)
  list_dbs = Mixlib::ShellOut.new('sudo -u postgres psql postgres -l')
  list_dbs.run_command
  list_dbs.stdout.include?(database) ? true : false
end

def username_created?(username)
  list_users = Mixlib::ShellOut.new('sudo -u postgres psql postgres -c "\\du"')
  list_users.run_command
  list_users.stdout.include?(username) ? true : false
end

def privs_granted?(username)
  grants = Mixlib::ShellOut.new('sudo -u postgres psql postgres -l')
  grants.run_command
  grants.stdout.include?("#{username}=CTc/postgres") ? true : false
end

def create_database?(database)
  create_database = Mixlib::ShellOut.new("sudo -u postgres psql postgres -c \"CREATE DATABASE #{database}\"")
  create_database.run_command
  create_database.stdout == 'CREATE DATABASE' ? true : false
end

def create_user?(username, password)
  create_user = Mixlib::ShellOut.new("sudo -u postgres psql postgres -c \"CREATE USER #{username} WITH PASSWORD \'#{password}\'\"")
  create_user.run_command
  create_user.stdout == 'CREATE ROLE' ? true : false
end

def grant_privileges?(database, username)
  grant_privileges = Mixlib::ShellOut.new("sudo -u postgres psql postgres -c \"GRANT ALL PRIVILEGES ON DATABASE #{database} to #{username}\"")
  grant_privileges.run_command
  grant_privileges.stdout == 'GRANT' ? true : false
end

# Replication User functions
def replicataion_user_created?
  list_users = Mixlib::ShellOut.new('sudo -u postgres psql postgres -c "\\du"')
  list_users.run_command
  list_users.stdout.include?('pg_rep_user') ? true : false
end

def create_replication_user?(password)
  create_replication_user = Mixlib::ShellOut.new("sudo -u postgres psql postgres -c \"CREATE USER pg_rep_user WITH PASSWORD \'#{password}\' CONNECTION LIMIT 5 REPLICATION\"")
  create_replication_user.run_command
  create_replication_user.stdout == 'CREATE ROLE' ? true : false
end
