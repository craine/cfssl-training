#
# Cookbook Name:: postgresql
# Recipe:: ha-slave
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

db_info_hash = node['pki']['db_info']

# Setup pg_hba.conf options
node.default['postgresql']['pg_hba'] = [
  {
    comment: '# Allow the user that is being created for this server to access locally',
    type: 'local',
    db: db_info_hash['database']['db_name'],
    user: db_info_hash['database']['username'],
    addr: nil,
    method: 'password'
  },
  {
    comment: '# Including all of the RFC 1918 networks as well ',
    type: 'hostssl',
    db: db_info_hash['database']['db_name'],
    user: db_info_hash['database']['username'],
    addr: '10.0.0.0/8',
    method: 'password'
  },
  {
    type: 'hostssl',
    db: db_info_hash['database']['db_name'],
    user: db_info_hash['database']['username'],
    addr: '172.16.0.0/12',
    method: 'password'
  },
  {
    type: 'hostssl',
    db: db_info_hash['database']['db_name'],
    user: db_info_hash['database']['username'],
    addr: '192.168.0.0/16',
    method: 'password'
  },
  {
    comment: 'Replication user to slave',
    type: 'hostssl',
    db: 'replication',
    user: 'pg_rep_user',
    addr: db_info_hash['ha']['master_node'],
    method: 'md5'
  }
]

# Install PostgreSQL and start its services
include_recipe 'postgresql::server'

# Stop PostgreSQL so we can set up the needed replication items
service 'postgresql' do
  action :stop
end

master_host = if db_info_hash['ha']['master_node'].slice(db_info_hash['ha']['master_node'].length - 3, 3) == '/32'
                db_info_hash['ha']['master_node'].slice(0, db_info_hash['ha']['master_node'].length - 3)
              else
                db_info_hash['ha']['master_node']
              end

template '/var/lib/postgresql/.pgpass' do
  source 'pgpass.erb'
  owner 'postgres'
  group 'postgres'
  mode '0600'
  variables hostname: master_host,
            db_name: '*',
            password: db_info_hash['ha']['replication_password']
end

# Store postgres version number
postgres_version = node['platform_version'] == '14.04' ? 9.3 : 'platform_not_defined'

# Move the postgres datafile out of the way so we can intiate backups
execute 'backup_pg_data_file' do
  command "mv /var/lib/postgresql/#{postgres_version}/main /var/lib/postgresql/#{postgres_version}/main_old"
  not_if { ::File.exist?("/var/lib/postgresql/#{postgres_version}/main_old") }
end

# Run the pg_backup utility
execute 'pg_backup' do
  command "sudo -u postgres pg_basebackup -h #{master_host} -D /var/lib/postgresql/#{postgres_version}/main -U pg_rep_user -v -P --xlog-method=stream"
  not_if  { ::File.exist?("/var/lib/postgresql/#{postgres_version}/main") }
end

# Create the replication configuration file
template "/var/lib/postgresql/#{postgres_version}/main/recovery.conf" do
  source 'recovery.conf.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables hostname: master_host,
            password: db_info_hash['ha']['replication_password']
end

# Stop PostgreSQL so we can set up the needed replication items
service 'postgresql' do
  action :start
end
