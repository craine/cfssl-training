#
# Cookbook Name:: postgresql
# Recipe:: ha-master
#
# Copyright 2017 Craine Runton
#
# Licensed under the MIT License. See /LICENSE.md for complete terms
#

db_info_hash = node['pki']['db_info']

# Setup pg_hba.conf options
node.default['postgresql']['pg_hba'] = [
  {
    comment: '# Allow the user that is being created for this server to access locally',
    type: 'local',
    db: db_info_hash['database']['db_name'],
    user: db_info_hash['database']['username'],
    addr: nil,
    method: 'password'
  },
  {
    comment: '# Including all of the RFC 1918 networks as well ',
    type: 'hostssl',
    db: db_info_hash['database']['db_name'],
    user: db_info_hash['database']['username'],
    addr: '10.0.0.0/8',
    method: 'password'
  },
  {
    type: 'hostssl',
    db: db_info_hash['database']['db_name'],
    user: db_info_hash['database']['username'],
    addr: '172.16.0.0/12',
    method: 'password'
  },
  {
    type: 'hostssl',
    db: db_info_hash['database']['db_name'],
    user: db_info_hash['database']['username'],
    addr: '192.168.0.0/16',
    method: 'password'
  },
  {
    comment: 'Replication user to slave',
    type: 'hostssl',
    db: 'replication',
    user: 'pg_rep_user',
    addr: db_info_hash['ha']['slave_node'],
    method: 'md5'
  }
]

# Install PostgreSQL and start its services
include_recipe 'postgresql::server'

# Store postgres version number
postgres_version = node['platform_version'] == '14.04' ? 9.3 : 'platform_not_defined'

return if postgres_version == 'platform_not_defined'

# Create the database that will be used by the application
postgres_ha db_info_hash['database']['db_name'] do
  username db_info_hash['database']['username']
  password db_info_hash['database']['password']
  action :bootstrap_db
  not_if { login_available? db_info_hash['database']['db_name'], db_info_hash['database']['username'] }
end

# Create the replication user for communication between master and slave servers
postgres_ha 'replication' do
  password db_info_hash['ha']['replication_password']
  action :replication_user
  not_if { replicataion_user_created? }
end

# Create the folder that archive data will be saved to
directory "/var/lib/postgresql/#{postgres_version}/main/mnt/server/archivedir" do
  owner 'postgres'
  group 'postgres'
  mode '0700'
  recursive true
  action :create
end
