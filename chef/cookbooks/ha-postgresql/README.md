ha-postgresql Cookbook
================

Installs PostgreSQL server in an HA setup

Requirements
------------

**Clustered nodes requires using Ubuntu 14.04 or higher**

Usage
-----
## `ha-postgresql::ha-master`
### Purpose
This recipe sets up a master PostgreSQL node

### Usage
Once the server is bootstrapped, perform the following steps

### Edit `db_info_hash`
```
{
  "database": {
    "db_name":"{{database_name}}",
    "username": "{{db_username}}",
    "password":"{{db_password}}"
  },
  "ha": {
    "master_node": "{{hostname_for_master_node}}",
    "slave_node": "{{hostname_for_slave_node}}",
    "replication_password": "{{password_for_replication_user}}"
  }
}
```
* The `master_node` and `slave_node` parameters can take either a hostname or an IP. **If you use an IP address, you *must* include the netmask of `/32` at the end of the IP.**


## `ha-postgresql::ha-slave`
### Purpose
This recipe sets up a slave PostgreSQL node

### Edit `db_info_hash`
```
{
  "database": {
    "db_name":"{{database_name}}",
    "username": "{{db_username}}",
    "password":"{{db_password}}"
  },
  "ha": {
    "master_node": "{{hostname_for_master_node}}",
    "slave_node": "{{hostname_for_slave_node}}",
    "replication_password": "{{password_for_replication_user}}"
  }
}
```  
* The `master_node` and `slave_node` parameters can take either a hostname or an IP. **If you use an IP address, you *must* include the netmask of `/32` at the end of the IP.**
