#!/bin/bash

# Install Chef-client
echo ">>>> Installing Chef-client <<<<"
cd /tmp
wget https://packages.chef.io/files/stable/chef/12.18.31/ubuntu/14.04/chef_12.18.31-1_amd64.deb;sudo dpkg -i chef_12.18*;

mkdir -p /opt/chef/cookbooks && cd /opt/chef/cookbooks
git init && touch readme.txt && git add --all && git commit -a -m "commit" && git stash save
cp -R /opt/cfssl-training/chef/cookbooks/* /opt/chef/cookbooks

knife supermarket install postgresql 6.0.1 -o /opt/chef/cookbooks
knife supermarket install database 6.1.1 -o /opt/chef/cookbooks

cp /opt/cfssl-training/chef/client.rb /opt/chef/client.rb
