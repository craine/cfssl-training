#!/bin/bash
##########################################################################################
# Copyright 2017 Craine Runton craine@runtondev.com
# Licensed under the MIT License. See /LICENSE.md for complete terms
#
# Installs the certificate in the script into the Linux System Keystore
##########################################################################################

read -d '' CERT <<"EOF"
-----BEGIN CERTIFICATE-----
MIIFwTCCA6mgAwIBAgIJAKrMNCAvoWwnMA0GCSqGSIb3DQEBCwUAMHcxCzAJBgNV
BAYTAlVTMREwDwYDVQQIDAhDb2xvcmFkbzEPMA0GA1UEBwwGRGVudmVyMRkwFwYD
VQQKDBBFeGFtcGxlIE9yZywgTHRkMQswCQYDVQQLDAJJVDEcMBoGA1UEAwwTcm9v
dC1jYS5leGFtcGxlLmNvbTAeFw0xNzAyMTgyMDMwNDVaFw0yNzAyMTYyMDMwNDVa
MHcxCzAJBgNVBAYTAlVTMREwDwYDVQQIDAhDb2xvcmFkbzEPMA0GA1UEBwwGRGVu
dmVyMRkwFwYDVQQKDBBFeGFtcGxlIE9yZywgTHRkMQswCQYDVQQLDAJJVDEcMBoG
A1UEAwwTcm9vdC1jYS5leGFtcGxlLmNvbTCCAiIwDQYJKoZIhvcNAQEBBQADggIP
ADCCAgoCggIBALRTM5jCY64raKCfKXFLXD2qZl6+xaEd6X5RekUjjwXLWzYOY8MV
0O4dqfrUF763DsGUHsOWzZla+JETE7xgWX2JW90K1M/ghLr2K0Q9m90DMnRKIRax
5nbC+5oF+EjOY3O7VKAjmup6XndTPTTkWdBeqfl01RocN4SbxQ/tmdTmpkQfPTDS
gXTZcMQV4TtqQSR5FiVah1dPC0PLeElzCJLEQjY3z6rgdTIsvvYbJ6BOhg+ErHGk
ufnmcyUpOcQ1e+xQ7l+stWc+lF2jeGnWLmeslhUfL/KWjjwxuMXzyo3oBjKs1jtd
tdVSge6C25bbEKQLzraJgz5kLpx5z4MTFi675rWjEkCoq/LE7sONLmlF/ZJ/f5fr
8N+m/x/ZpZSkPdhFyzc1qLGMuhUf+RJonemUbF6pt2ZyTD/jiyUUlVjQmrbN22Fu
yKYUru5//tADTbRhTtRacUJJjoAB6QnLuI74sYfPPB6ZJC3YjMERHfF7k70q35eR
7/cMc/dvkkgCEbjkEa6vhAQE/KIOjJbBdyaIYokrfTJ44cw2hiB8gtB6fX63xdr0
XbjY7uC2zO2UXxi3QDyI/NDeLK2N0wBFJ/az0mxvgixfbP66aDZP7VVEfO/l8+AL
4uo6WvP3dYaeLuEC4oyp+sHiLaI07PJeJ5TwE1kjCW3B8KnaJ3cUmO1bAgMBAAGj
UDBOMB0GA1UdDgQWBBTV4t9+QcIgK+BZx+aUmxjxPwronTAfBgNVHSMEGDAWgBTV
4t9+QcIgK+BZx+aUmxjxPwronTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBCwUA
A4ICAQAk6VG0z1gRU4DyVOP6FHqBGUhw29s2hjZYkAzx5aROT4ZScYeVqmYEXE6x
FV0cKLZVQ9FqATBo+F5FVwvSVi0IiT8N0AK14Y6VIgPtzS4zVRE9EdA7mo0M+Vtb
ovcGBqxQCMi8hNMSSe6EqBEID3gOwb6eEyX6BKVkYlFENji96A31kHJTYsrxXScc
JccVKhJumo80mAAquvCg0HUDU0KfhBk0Fd80hfsPB3sYZQt+pVgfL7Aoc4P8HiSp
sKB4CftEWZFeK/xvrawD69BHKkTDJdkCLC3GkUeuw1Q1h403I3nEfVzE65ojeyKP
erAXRXlWPqRx8pRGzlCPCzihCmJUT1riwTRKvcByb+bjiGGunQbhGRphHY0T4s/p
rms4Ae2ax01vOfrL56/UeHxyDMLHmi37rhLt44JwZZp8QJAwr5n6aoFadv3DlB8q
cc3jUrACi/CTNhNVn7i3mfnb1LrtvkU9axcZBO3+OcQ2AtvhGV0EES8z0ZVxT5Yi
CwO60DYuaYgJscvmE5xFIztITa0FTqgBBzsi/8tpjDC4EsieZqdJ9pq/7tW8M8Ch
mgDQakkMIxbqCT/DjdlXBCBNMQohG6iCuVZI1ghLhWdOA/pLTV1GSQy1sLb/a//b
B4aNFYh1YYr5Wtact5OtkblPrcD5lUFn+D3PCewVqDJECsIx5A==
-----END CERTIFICATE-----
EOF
echo "$CERT" > /tmp/root_ca.crt

mkdir -p /usr/local/share/ca-certificates/example-org/
if [ -e '/usr/local/share/ca-certificates/example-org/root_ca.crt' ]
then
  OLD_PEM=$(openssl sha1 /usr/local/share/ca-certificates/example-org/root_ca.crt)
  OLD_PEM=${OLD_PEM: -40}
  NEW_PEM=$(openssl sha1 /tmp/root_ca.crt)
  NEW_PEM=${NEW_PEM: -40}

  if [ $OLD_PEM != $NEW_PEM ]
  then
    mv /tmp/root_ca.crt /usr/local/share/ca-certificates/example-org/root_ca.crt
    update-ca-certificates
    rm -rf /tmp/root_ca.crt

  fi
else
  mv /tmp/root_ca.crt /usr/local/share/ca-certificates/example-org/root_ca.crt
  update-ca-certificates
  rm -rf /tmp/root_ca.crt

fi
