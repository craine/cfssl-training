REM#########################################################################################
REM Copyright 2017 Craine Runton craine@runtondev.com
REM Licensed under the MIT License. See /LICENSE.md for complete terms
REM
REM Installs the certificate in the script into the Windows Trusted Root Certification Authorities
REM#########################################################################################

ECHO -----BEGIN CERTIFICATE----- >> %temp%\root_ca.pem
ECHO MIIFwTCCA6mgAwIBAgIJAKrMNCAvoWwnMA0GCSqGSIb3DQEBCwUAMHcxCzAJBgNV >> %temp%\root_ca.pem
ECHO BAYTAlVTMREwDwYDVQQIDAhDb2xvcmFkbzEPMA0GA1UEBwwGRGVudmVyMRkwFwYD >> %temp%\root_ca.pem
ECHO VQQKDBBFeGFtcGxlIE9yZywgTHRkMQswCQYDVQQLDAJJVDEcMBoGA1UEAwwTcm9v >> %temp%\root_ca.pem
ECHO dC1jYS5leGFtcGxlLmNvbTAeFw0xNzAyMTgyMDMwNDVaFw0yNzAyMTYyMDMwNDVa >> %temp%\root_ca.pem
ECHO MHcxCzAJBgNVBAYTAlVTMREwDwYDVQQIDAhDb2xvcmFkbzEPMA0GA1UEBwwGRGVu >> %temp%\root_ca.pem
ECHO dmVyMRkwFwYDVQQKDBBFeGFtcGxlIE9yZywgTHRkMQswCQYDVQQLDAJJVDEcMBoG >> %temp%\root_ca.pem
ECHO A1UEAwwTcm9vdC1jYS5leGFtcGxlLmNvbTCCAiIwDQYJKoZIhvcNAQEBBQADggIP >> %temp%\root_ca.pem
ECHO ADCCAgoCggIBALRTM5jCY64raKCfKXFLXD2qZl6+xaEd6X5RekUjjwXLWzYOY8MV >> %temp%\root_ca.pem
ECHO 0O4dqfrUF763DsGUHsOWzZla+JETE7xgWX2JW90K1M/ghLr2K0Q9m90DMnRKIRax >> %temp%\root_ca.pem
ECHO 5nbC+5oF+EjOY3O7VKAjmup6XndTPTTkWdBeqfl01RocN4SbxQ/tmdTmpkQfPTDS >> %temp%\root_ca.pem
ECHO gXTZcMQV4TtqQSR5FiVah1dPC0PLeElzCJLEQjY3z6rgdTIsvvYbJ6BOhg+ErHGk >> %temp%\root_ca.pem
ECHO ufnmcyUpOcQ1e+xQ7l+stWc+lF2jeGnWLmeslhUfL/KWjjwxuMXzyo3oBjKs1jtd >> %temp%\root_ca.pem
ECHO tdVSge6C25bbEKQLzraJgz5kLpx5z4MTFi675rWjEkCoq/LE7sONLmlF/ZJ/f5fr >> %temp%\root_ca.pem
ECHO 8N+m/x/ZpZSkPdhFyzc1qLGMuhUf+RJonemUbF6pt2ZyTD/jiyUUlVjQmrbN22Fu >> %temp%\root_ca.pem
ECHO yKYUru5//tADTbRhTtRacUJJjoAB6QnLuI74sYfPPB6ZJC3YjMERHfF7k70q35eR >> %temp%\root_ca.pem
ECHO 7/cMc/dvkkgCEbjkEa6vhAQE/KIOjJbBdyaIYokrfTJ44cw2hiB8gtB6fX63xdr0 >> %temp%\root_ca.pem
ECHO XbjY7uC2zO2UXxi3QDyI/NDeLK2N0wBFJ/az0mxvgixfbP66aDZP7VVEfO/l8+AL >> %temp%\root_ca.pem
ECHO 4uo6WvP3dYaeLuEC4oyp+sHiLaI07PJeJ5TwE1kjCW3B8KnaJ3cUmO1bAgMBAAGj >> %temp%\root_ca.pem
ECHO UDBOMB0GA1UdDgQWBBTV4t9+QcIgK+BZx+aUmxjxPwronTAfBgNVHSMEGDAWgBTV >> %temp%\root_ca.pem
ECHO 4t9+QcIgK+BZx+aUmxjxPwronTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBCwUA >> %temp%\root_ca.pem
ECHO A4ICAQAk6VG0z1gRU4DyVOP6FHqBGUhw29s2hjZYkAzx5aROT4ZScYeVqmYEXE6x >> %temp%\root_ca.pem
ECHO FV0cKLZVQ9FqATBo+F5FVwvSVi0IiT8N0AK14Y6VIgPtzS4zVRE9EdA7mo0M+Vtb >> %temp%\root_ca.pem
ECHO ovcGBqxQCMi8hNMSSe6EqBEID3gOwb6eEyX6BKVkYlFENji96A31kHJTYsrxXScc >> %temp%\root_ca.pem
ECHO JccVKhJumo80mAAquvCg0HUDU0KfhBk0Fd80hfsPB3sYZQt+pVgfL7Aoc4P8HiSp >> %temp%\root_ca.pem
ECHO sKB4CftEWZFeK/xvrawD69BHKkTDJdkCLC3GkUeuw1Q1h403I3nEfVzE65ojeyKP >> %temp%\root_ca.pem
ECHO erAXRXlWPqRx8pRGzlCPCzihCmJUT1riwTRKvcByb+bjiGGunQbhGRphHY0T4s/p >> %temp%\root_ca.pem
ECHO rms4Ae2ax01vOfrL56/UeHxyDMLHmi37rhLt44JwZZp8QJAwr5n6aoFadv3DlB8q >> %temp%\root_ca.pem
ECHO cc3jUrACi/CTNhNVn7i3mfnb1LrtvkU9axcZBO3+OcQ2AtvhGV0EES8z0ZVxT5Yi >> %temp%\root_ca.pem
ECHO CwO60DYuaYgJscvmE5xFIztITa0FTqgBBzsi/8tpjDC4EsieZqdJ9pq/7tW8M8Ch >> %temp%\root_ca.pem
ECHO mgDQakkMIxbqCT/DjdlXBCBNMQohG6iCuVZI1ghLhWdOA/pLTV1GSQy1sLb/a//b >> %temp%\root_ca.pem
ECHO B4aNFYh1YYr5Wtact5OtkblPrcD5lUFn+D3PCewVqDJECsIx5A== >> %temp%\root_ca.pem
ECHO -----END CERTIFICATE----- >> %temp%\root_ca.pem

certutil -addstore -enterprise -f "Root" %temp%\root_ca.pem
certutil -addstore -enterprise -f "trustedpublisher" %temp%\root_ca.pem
del %temp%\root_ca.pem
