# CFSSL Training
## About this repo
**Author:** Craine Runton

This repository contains the material needed for use in the _"Building an Internal PKI using Open Source Tools"_ training that Craine Runton gives periodically.

## Organization
```
cfssl-training/
├── cfssl/
|   ├── root-ca.example.com/
|   ├── intermediate-ca-01.example.com/
|   ├── issuing-ca-0101.example.com/
|   └── cfssl.tar.gz
├── chef/
|   ├── cookbooks/
|       ├── cfssl-pki/
|       └── ha-postgresql/
├── digitalocean/
└── scripts
```
