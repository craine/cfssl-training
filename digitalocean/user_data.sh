#!/bin/bash

# Update system and install git
apt-get update
apt-get install -y git

cd /root
cat <<EOF > /root/.bash_profile
PROMPT_COMMAND='echo -ne "\033]0;\${USER}@\$(hostname -f)\007"'
HISTFILE=\$HOME/.bash_history
alias ll="ls -alh --color"
EOF

cat <<EOF > /root/.gitconfig
[user]
	email = you@example.com
	name = Name
EOF

cat <<EOF >> /root/.ssh/known_hosts
gitlab.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=
EOF

cd /opt
git clone https://gitlab.com/craine/cfssl-training.git
chmod +x /opt/cfssl-training/scripts/*

mkdir /opt/cfssl
tar -xvf /opt/cfssl-training/cfssl/cfssl.tar.gz -C /opt/cfssl
cat <<EOF >> /root/.bash_profile
export PATH=\$PATH:/opt/cfssl/bin
EOF

mkdir /opt/cfssl/example-org
