# Server Setup
##Create 9 new droplets
We will need 9 droplets to complete all sections of the training course. In DigitalOcean, you will need to create all 9 of these server the night before your training. It will only take you about 10 minutes to complete.

8 of the servers have the same specs, as follows:

| Item                | Spec                                  |
|---------------------|---------------------------------------|
| Image               | Ubuntu 14.04.5 x64                    |
| Size                | 512 MB / 1 CPU                        |
| Data Center Region  | NYC or SFO                            |
| Additional Options  | Private networking<br>User data (see ./user_data.sh)<br>Monitoring |
| Hostname 1          | root-ca.example.com                   |
| Hostname 2          | intermediate-ca-01.example.com        |
| Hostname 3          | issuing-ca-0101.example.com           |
| Hostname 4          | postgre-master.example.com            |
| Hostname 5          | postgre-slave.example.com             |
| Hostname 6          | server-entity.example.com             |
| Hostname 7          | server-entity-additional.example.com  |
| Hostname 8          | aia-server.example.com                |


The final server has the following specs:

| Item                | Spec                                  |
|---------------------|---------------------------------------|
| Image               | Ubuntu 14.04.5 x64                    |
| Size                | 2 GB / 2 CPU                          |
| Data Center Region  | NYC or SFO                            |
| Additional Options  | Private networking<br>User data (see ./user_data.sh)<br>Monitoring |
| Hostname 1          | cfssl-build.example.com               |
